﻿using MailKit.Net.Pop3;
using MailLib.Models;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailLib
{
    public class POP3Connector
    {
        public Pop3Client Client { get; private set; }
        public bool IsConnected { get; private set; }
        public void Dispose()
        {
            var taskResult = Task.Run<Pop3LogoutResult>(async () =>
            {
                var result = await Logout();

                return result;
            });
            taskResult.Wait();
        }

        public async Task<Pop3LogoutResult> Logout()
        {
            var taskResult = await Task.Run<Pop3LogoutResult>(() =>
            {

                var result = new Pop3LogoutResult
                {
                    IsOk = true
                };

                try
                {
                    if (Client != null && IsConnected)
                    {
                        Client.Disconnect(true);
                    }
                }
                catch (Exception ex)
                {
                    result.IsOk = false;
                    result.Message = ex.Message;

                }

                return result;
            });

            return taskResult;
        }

        public async Task<Pop3LoginResult> Login(Pop3LoginRequest loginRequest)
        {
            var taskResult = await Task.Run<Pop3LoginResult>(() =>
            {
                var result = new Pop3LoginResult
                {
                    IsOk = true
                };

                try
                {
                    Client = new Pop3Client();
                    Client.Connected += Client_Connected;
                    Client.Disconnected += Client_Disconnected;

                    Client.Connect(loginRequest.Server, loginRequest.Port, loginRequest.SecureSocketOptions);

                    Client.Authenticate(loginRequest.Username, loginRequest.Password);
                }
                catch (Exception ex)
                {
                    result.IsOk = false;
                    result.Message = ex.Message;

                }

                return result;
            });

            return taskResult;
        }

        public async Task<Pop3GetMailResult> GetMails()
        {
            var taskResult = await Task.Run<Pop3GetMailResult>(() =>
            {
                var result = new Pop3GetMailResult
                {
                    IsOk = true,
                    Messages = new List<MimeMessage>()
                };

                if (Client == null || !IsConnected)
                {
                    result.IsOk = false;
                    return result;
                }

                var mailCount = Client.Count;

                var mailIndexes = new List<int>();
                for (int i = 0; i < mailCount; i++)
                {
                    var message = Client.GetMessage(i);
                    result.Messages.Add(message);
                    mailIndexes.Add(i);
                }

                
                return result;
            });

            return taskResult;
        }

        public async Task<Pop3DeleteAllMailsResult> DeleteAllMails()
        {
            var taskResult = await Task.Run<Pop3DeleteAllMailsResult>(() =>
            {
                var result = new Pop3DeleteAllMailsResult
                {
                    IsOk = true
                };

                if (Client == null || !IsConnected)
                {
                    result.IsOk = false;
                    return result;
                }

                for (int i = 0; i < Client.Count; i++)
                {
                    Client.DeleteMessage(i);
                }
                

                return result;
            });

            return taskResult;
        }

        private void Client_Disconnected(object sender, EventArgs e)
        {
            IsConnected = false;
        }

        private void Client_Connected(object sender, EventArgs e)
        {
            IsConnected = true;
        }
    }
}
