﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailLib.Models
{
    public class Pop3DeleteAllMailsResult
    {
        public bool IsOk { get; set; }
        public string Message { get; set; }
    }
}
