﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailLib.Models
{
    public class ImapGetMailsRequest
    {
        public bool DeleteMails { get; set; }

        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }

        public IMessageOutput MessageOutput { get; set; }
    }
}
