﻿using MailKit.Net.Pop3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailLib.Models
{
    public class Pop3LoginResult
    {
        public bool IsOk { get; set; }
        public string Message { get; set; }
        public Pop3Client Client { get; set; }
    }
}
