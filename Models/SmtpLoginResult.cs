﻿using MailKit.Net.Smtp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailLib.Models
{
    public class SmtpLoginResult
    {
        public bool IsOk { get; set; }
        public string Message { get; set; }
        public SmtpClient Client { get; set; }
    }
}
