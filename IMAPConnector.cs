﻿using MailKit;
using MailKit.Net.Imap;
using MailLib.Models;
using MimeKit;
using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MailLib
{
    public delegate bool SaveMailsHandler(List<MimeMessage> mimeMessages, IMessageOutput messageOutput, string elasticServer, int elasticPort, string elasticIndex, string elasticType, string attachementStorePath, List<object> mails);
    public class IMAPConnector : IDisposable
    {
        public ImapClient Client { get; private set; }
        public bool IsConnected { get; private set; }
        public event SaveMailsHandler SaveMails;
        public void Dispose()
        {
            var taskResult = Task.Run<ImapLogoutResult>(async () =>
            {
                var result = await Logout();

                return result;
            });
            taskResult.Wait();
        }

        public async Task<ImapLogoutResult> Logout()
        {
            var taskResult = await Task.Run<ImapLogoutResult>(() =>
            {

                var result = new ImapLogoutResult
                {
                    IsOk = true
                };

                try
                {
                    if (Client != null && IsConnected)
                    {
                        Client.Disconnect(true);
                    }
                }
                catch (Exception ex)
                {
                    result.IsOk = false;
                    result.Message = ex.Message;

                }

                return result;
            });

            return taskResult;
        }

        public async Task<ImapLoginResult> Login(ImapLoginRequest loginRequest)
        {
            var taskResult = await Task.Run<ImapLoginResult>(() =>
            {
                var result = new ImapLoginResult
                {
                    IsOk = true
                };

                try
                {
                    Client = new ImapClient();
                    Client.Connected += Client_Connected;
                    Client.Disconnected += Client_Disconnected;

                    if (loginRequest.ValidateServerCertificate)
                    {
                        Client.ServerCertificateValidationCallback = (s, c, h, e) => loginRequest.ValidateServerCertificate;
                    }

                    Client.Connect(loginRequest.Server, loginRequest.Port, loginRequest.UseSSL);

                    Client.Authenticate(loginRequest.Username, loginRequest.Password);
                }
                catch (Exception ex)
                {
                    result.IsOk = false;
                    result.Message = ex.Message;

                }

                return result;
            });

            return taskResult;
        }

        public async Task<ImapGetMailResult> GetMailsOfInbox(ImapGetMailsRequest getMailsRequest)
        {
            var taskResult = await Task.Run<ImapGetMailResult>(async () =>
            {
                var result = new ImapGetMailResult
                {
                    IsOk = true,
                    Messages = new List<MimeMessage>()
                };

                if (Client == null || !IsConnected)
                {
                    result.IsOk = false;
                    return result;
                }

                var folder = Client.Inbox;

                getMailsRequest.MessageOutput?.OutputInfo($"Open Mailfolder {folder.FullName}");
                folder.Open(MailKit.FolderAccess.ReadOnly);

                var mailCount = folder.Count;

                var mailIndexes = new List<int>();
                getMailsRequest.MessageOutput?.OutputInfo($"Found {mailCount} Mails");
                for (int i = 0; i < mailCount; i++)
                {
                    result.Messages.Add(folder.GetMessage(i));
                    mailIndexes.Add(i);
                }

                if (getMailsRequest.DeleteMails && mailIndexes.Any())
                {
                    Client.Inbox.AddFlags(mailIndexes.ToArray(), MailKit.MessageFlags.Deleted, true);
                    Client.Inbox.Expunge();
                }

                var getSubFolderResult = await GetMailsOfSubfolders(getMailsRequest, folder);
                result.IsOk = getSubFolderResult.IsOk;
                result.Message = getSubFolderResult.Message;
                result.Messages.AddRange(getSubFolderResult.Messages);

                return result;
            });

            return taskResult;
        }

        public async Task<ImapGetMailResult> GetMailsOfAllFolders(ImapGetMailsRequest getMailsRequest, 
            string elasticServer, 
            int elasticPort, 
            string elasticIndex, 
            string elasticType, 
            string attachementStorePath, 
            List<object> savedMails,
            CancellationToken cancellationToken)
        {
            var taskResult = await Task.Run<ImapGetMailResult>(() =>
            {
                var result = new ImapGetMailResult
                {
                    IsOk = true,
                    Messages = new List<MimeMessage>()
                };

                if (Client == null || !IsConnected)
                {
                    result.IsOk = false;
                    return result;
                }

                var folders = Client.GetFolders(Client.PersonalNamespaces[0]);

                foreach (var folder in folders)
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        getMailsRequest.MessageOutput?.OutputInfo($"Cancelled by User!");
                        return result;
                    }
                    folder.Open(MailKit.FolderAccess.ReadWrite);

                    var mailCount = folder.Count;
                    getMailsRequest.MessageOutput?.OutputInfo($"Found Folder {folder.FullName} ({mailCount} Mails)");

                    var mailIndexes = new List<int>();
                    var messagesToSave = new List<MimeMessage>();
                    for (int i = 0; i < mailCount; i++)
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            getMailsRequest.MessageOutput?.OutputInfo($"Cancelled by User!");
                            return result;
                        }
                        try
                        {
                            var message = folder.GetMessage(i);
                            if (getMailsRequest.Start != null && getMailsRequest.End != null)
                            {
                                if (message.Date < getMailsRequest.Start || message.Date > getMailsRequest.End)
                                {
                                    continue;
                                }
                            }
                            result.Messages.Add(message);
                            mailIndexes.Add(i);
                            messagesToSave.Add(message);
                        }
                        catch (Exception ex)
                        {

                        }
                        if ((result.Messages.Count % 100) == 0)
                        {
                            getMailsRequest.MessageOutput?.OutputInfo($"Get {result.Messages.Count} Mails");
                            var saveResult = SaveMails?.Invoke(messagesToSave, getMailsRequest.MessageOutput, elasticServer, elasticPort, elasticIndex, elasticType, attachementStorePath, savedMails);
                            if ((saveResult ?? false))
                            {
                                result.IsOk = false;
                                result.Message = "Save was not successful!";
                            }
                            messagesToSave.Clear();
                        }
                    }
                    getMailsRequest.MessageOutput?.OutputInfo($"Get {result.Messages.Count} Mails");

                    if (messagesToSave.Any())
                    {
                        var saveResult = SaveMails?.Invoke(messagesToSave, getMailsRequest.MessageOutput, elasticServer, elasticPort, elasticIndex, elasticType, attachementStorePath, savedMails);
                        if (!(saveResult ?? false))
                        {
                            result.IsOk = false;
                            result.Message = "Save was not successful!";
                            getMailsRequest.MessageOutput?.OutputError(result.Message);
                        }
                        messagesToSave.Clear();
                    }

                    if (getMailsRequest.DeleteMails && mailIndexes.Any())
                    {
                        folder.AddFlags(mailIndexes.ToArray(), MailKit.MessageFlags.Deleted, true);
                        folder.Expunge();
                    }

                    //var subFolderResult = await GetMailsOfSubfolders(getMailsRequest, folder);
                    //result.IsOk = subFolderResult.IsOk;
                    //result.Message = subFolderResult.Message;
                    //if (!result.IsOk)
                    //{
                    //    return result;
                    //}

                    //result.Messages.AddRange(subFolderResult.Messages);
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ImapGetMailResult> GetMailsOfSubfolders(ImapGetMailsRequest getMailsRequest, IMailFolder mailFolder)
        {
            var taskResult = await Task.Run<ImapGetMailResult>(async() =>
            {
                var result = new ImapGetMailResult
                {
                    IsOk = true,
                    Messages = new List<MimeMessage>()
                };

                if (Client == null || !IsConnected)
                {
                    result.IsOk = false;
                    return result;
                }
                
                var subFolders = mailFolder.GetSubfolders(false).ToList();

                foreach(var subFolder in subFolders)
                {
                    getMailsRequest.MessageOutput?.OutputInfo($"Found Folder {subFolder.FullName}");
                    subFolder.Open(MailKit.FolderAccess.ReadWrite);

                    var mailCount = subFolder.Count;

                    var mailIndexes = new List<int>();
                    for (int i = 0; i < mailCount; i++)
                    {
                        result.Messages.Add(subFolder.GetMessage(i));
                        mailIndexes.Add(i);
                    }

                    if (getMailsRequest.DeleteMails && mailIndexes.Any())
                    {
                        subFolder.AddFlags(mailIndexes.ToArray(), MailKit.MessageFlags.Deleted, true);
                        subFolder.Expunge();
                    }

                    var subFolderResult = await GetMailsOfSubfolders(getMailsRequest, subFolder);
                    result.IsOk = subFolderResult.IsOk;
                    result.Message = subFolderResult.Message;
                    if (!result.IsOk)
                    {
                        return result;
                    }

                    result.Messages.AddRange(subFolderResult.Messages);
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ImapGetMailResult> GetMailsOfSendFolder(ImapGetMailsRequest getMailsRequest)
        {
            var taskResult = await Task.Run<ImapGetMailResult>(() =>
            {
                var result = new ImapGetMailResult
                {
                    IsOk = true,
                    Messages = new List<MimeMessage>()
                };

                if (Client == null || !IsConnected)
                {
                    result.IsOk = false;
                    return result;
                }
                if ((Client.Capabilities & (ImapCapabilities.SpecialUse | ImapCapabilities.XList)) == 0)
                {
                    return result;
                }

                var folder = Client.GetFolder(MailKit.SpecialFolder.Sent);

                folder.Open(MailKit.FolderAccess.ReadOnly);

                var mailCount = folder.Count;

                var mailIndexes = new List<int>();
                for (int i = 0; i < mailCount; i++)
                {
                    result.Messages.Add(folder.GetMessage(i));
                    mailIndexes.Add(i);
                }

                if (getMailsRequest.DeleteMails && mailIndexes.Any())
                {
                    Client.Inbox.AddFlags(mailIndexes.ToArray(), MailKit.MessageFlags.Deleted, true);
                    Client.Inbox.Expunge();
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ImapGetMailResult> GetMailsOfSpamFolder(ImapGetMailsRequest getMailsRequest)
        {
            var taskResult = await Task.Run<ImapGetMailResult>(() =>
            {
                var result = new ImapGetMailResult
                {
                    IsOk = true,
                    Messages = new List<MimeMessage>()
                };

                if (Client == null || !IsConnected)
                {
                    result.IsOk = false;
                    return result;
                }

                if ((Client.Capabilities & (ImapCapabilities.SpecialUse | ImapCapabilities.XList)) == 0)
                {
                    return result;
                }

                var folder = Client.GetFolder(MailKit.SpecialFolder.Junk);

                folder.Open(MailKit.FolderAccess.ReadOnly);

                var mailCount = folder.Count;

                var mailIndexes = new List<int>();
                for (int i = 0; i < mailCount; i++)
                {
                    result.Messages.Add(folder.GetMessage(i));
                    mailIndexes.Add(i);
                }

                if (getMailsRequest.DeleteMails && mailIndexes.Any())
                {
                    Client.Inbox.AddFlags(mailIndexes.ToArray(), MailKit.MessageFlags.Deleted, true);
                    Client.Inbox.Expunge();
                }

                return result;
            });

            return taskResult;
        }


        private void Client_Disconnected(object sender, EventArgs e)
        {
            IsConnected = false;
        }

        private void Client_Connected(object sender, EventArgs e)
        {
            IsConnected = true;
        }
    }
}
