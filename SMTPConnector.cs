﻿using MailKit.Net.Smtp;
using MailLib.Models;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailLib
{
    public class SMTPConnector
    {
        public SmtpClient Client { get; private set; }
        public bool IsConnected { get; private set; }
        public void Dispose()
        {
            var taskResult = Task.Run<SmtpLogoutResult>(async () =>
            {
                var result = await Logout();

                return result;
            });
            taskResult.Wait();
        }

        public async Task<SmtpLogoutResult> Logout()
        {
            var taskResult = await Task.Run<SmtpLogoutResult>(() =>
            {

                var result = new SmtpLogoutResult
                {
                    IsOk = true
                };

                try
                {
                    if (Client != null && IsConnected)
                    {
                        Client.Disconnect(true);
                    }
                }
                catch (Exception ex)
                {
                    result.IsOk = false;
                    result.Message = ex.Message;

                }

                return result;
            });

            return taskResult;
        }

        public async Task<SmtpLoginResult> Login(SmtpLoginRequest loginRequest)
        {
            var taskResult = await Task.Run<SmtpLoginResult>(() =>
            {
                var result = new SmtpLoginResult
                {
                    IsOk = true
                };

                try
                {
                    Client = new SmtpClient();
                    Client.Connected += Client_Connected;
                    Client.Disconnected += Client_Disconnected;

                    Client.Connect(loginRequest.Server, loginRequest.Port, loginRequest.SecureSocketOptions);

                    Client.Authenticate(loginRequest.Username, loginRequest.Password);
                }
                catch (Exception ex)
                {
                    result.IsOk = false;
                    result.Message = ex.Message;

                }

                return result;
            });

            return taskResult;
        }

        public async Task<SmtpSendMessageResult> SendMessage(MimeMessage message)
        {
            var taskResult = await Task.Run<SmtpSendMessageResult>(() =>
            {
                var result = new SmtpSendMessageResult
                {
                    IsOk = true
                };

                if (Client == null || !Client.IsConnected)
                {
                    result.IsOk = false;
                    return result;
                }

                try
                {
                    Client.Send(message);
                }
                catch (Exception ex)
                {

                    result.IsOk = false;
                    result.Message = ex.Message;
                }

                return result;
            });

            return taskResult;
        }

        private void Client_Disconnected(object sender, EventArgs e)
        {
            IsConnected = false;
        }

        private void Client_Connected(object sender, EventArgs e)
        {
            IsConnected = true;
        }
    }
}
